# -*- coding: utf-8 -*-

import datetime
import json
import urllib.request
import moment
from datetime import datetime


# ------------------------------------------------------------ Index and search method --------------------------------

def index():
    form = FORM('City', INPUT(_name='city'), INPUT(_type='submit'))
    # Print the city value

    print(request.vars.city)
    appid = "94c6cf0868fa5cb930a5e2d71baf0dbf"

    if request.vars.city:
        url = "http://api.openweathermap.org/data/2.5/forecast/daily?q=" + request.vars.city + "&mode=json&units=metric&cnt=10&appid=" + appid
        try:
            url = urllib.request.urlopen(url)
            output = url.read().decode('utf-8')
            data = json.loads(output)
            url.close()

            # print returned data

            print(data)
            labels = []
            graph_data = []
            for graph_x in data['list']:
                # labels.append(str(moment.unix(graph_x['dt']).format("ddd")))
                labels.append(moment.unix(graph_x['dt']).format("ddd"))
                graph_data.append(graph_x['temp']['day'])
                graph_data_json = json.dumps(graph_data)



        except:
            inval = "Not found"
            response.flash = T("Verify your City")
            print("not found")

        # Save data in the log
        db.weather_request_log.insert(city=request.vars.city)

    else:
        pass
    return locals()


# ---------------------------------------------- About the app --------------------------------------


def about():
    return locals()


# ---------------------------------------------- Search History -----------------------------------

@auth.requires_login()
def weatherlog():
    db.weather_request_log.id.readable = False
    db.weather_request_log.id.writable = False
    grid_log = SQLFORM.smartgrid(db.weather_request_log, create=False, details=False, editable=False, paginate=12, )

    return locals()


def user():
    return dict(form=auth())


# ---- action to server uploaded static content (required) ---
@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)
